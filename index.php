<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/reset.css">
    <link rel="stylesheet" type="text/css" href="style/interface.css">
    <link rel="stylesheet" type="text/css" href="style/main.css">
    <script src="polyfill.paged.js" type="text/javascript"></script>
    <title>CASO RIGOLO</title>

    <?php
      // Affiche les erreurs
      ini_set('display_errors', 1);
      ini_set('display_startup_errors', 1);
      error_reporting(E_ALL);

      require __DIR__ . '/vendor/autoload.php';

      $liste_fichiers = array("path"=>[], "basename"=>[], "dirname"=>[], "extension"=>[], "profondeur"=>[], "classes"=>[], "size"=>[], "mime"=>[], "categorie"=>[], "modif"=>[]);

      ////récupère le nom de tous les fichiers de l'arborescence, incluant les sous-dossiers
      function getSubDirectories($dir)
      {
          $subDir = array();
          //// Get and add directories of $dir
          //// $directories = array_filter(glob($dir), 'is_dir');
          $directories = glob($dir);
          $subDir = array_merge($subDir, $directories);
          //// Foreach directory, recursively get and add sub directories
          foreach ($directories as $directory) {
              $subDir = array_merge($subDir, getSubDirectories($directory.'/*'));
          }
          //// Return list of sub directories
          return $subDir;
      }

      ////pretty print pour les arrays/dictionnaires/gros objets
      function pr($var)
      {
          print '<pre>';
          print_r($var);
          print '</pre>';
      }

      /**
      * Converts bytes into human readable file size.
      *
      * @param string $bytes
      * @return string human readable file size (ex : 2,87 МB)
      * @author Mogilev Arseny
      */
      function FileSizeConvert($bytes) {
        $bytes = floatval($bytes);
          $arBytes = array(
            0 => array(
              "UNIT" => "To",
              "VALUE" => pow(1024, 4)
            ),
            1 => array(
              "UNIT" => "Go",
              "VALUE" => pow(1024, 3)
            ),
            2 => array(
              "UNIT" => "Mo",
              "VALUE" => pow(1024, 2)
            ),
            3 => array(
              "UNIT" => "Ko",
              "VALUE" => 1024
            ),
            4 => array(
              "UNIT" => "o",
              "VALUE" => 1
            ),
          );

        foreach($arBytes as $arItem) {
          if($bytes >= $arItem["VALUE"]) {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
          }
        }
        return $result;
      }

      //// traduit les noms des jours/mois en français
      function trad($string)
      {
        if (str_contains($string, 'Monday')) {
          $trad = str_replace('Monday', 'Lundi', $string);
        } else if (str_contains($string, 'Tuesday')) {
          $trad = str_replace('Tuesday', 'Mardi', $string);
        } else if (str_contains($string, 'Wednesday')) {
         $trad = str_replace('Wednesday', 'Mercredi', $string);
        } else if (str_contains($string, 'Thursday')) {
        $trad = str_replace('Thursday', 'Jeudi', $string);
        } else if (str_contains($string, 'Friday')) {
          $trad = str_replace('Friday', 'Vendredi', $string);
        } else if (str_contains($string, 'Satruday')) {
          $trad = str_replace('Satruday', 'Samedi', $string);
        } else if (str_contains($string, 'Sunday')) {
          $trad = str_replace('Sunday', 'Dimanche', $string);
        } else {
          $trad = $string;
        }

        if (str_contains($trad, 'January')) {
          $trad = str_replace('January', 'Janvier', $trad);
        } else if (str_contains($trad, 'February')) {
          $trad = str_replace('February', 'Février', $trad);
        } else if (str_contains($trad, 'March')) {
         $trad = str_replace('March', 'Mars', $trad);
        } else if (str_contains($trad, 'April')) {
          $trad = str_replace('April', 'Avril', $trad);
        } else if (str_contains($trad, 'May')) {
          $trad = str_replace('May', 'Mai', $trad);
        } else if (str_contains($trad, 'June')) {
          $trad = str_replace('June', 'Juin', $trad);
        } else if (str_contains($trad, 'July')) {
          $trad = str_replace('July', 'Juillet', $trad);
        } else if (str_contains($trad, 'August')) {
          $trad = str_replace('August', 'Août', $trad);
        } else if (str_contains($trad, 'September')) {
          $trad = str_replace('September', 'Septembre', $trad);
        } else if (str_contains($trad, 'October')) {
          $trad = str_replace('October', 'Octobre', $trad);
        } else if (str_contains($trad, 'November')) {
          $trad = str_replace('November', 'Novembre', $trad);
        } else if (str_contains($trad, 'December')) {
          $trad = str_replace('December', 'Décembre', $trad);
        }

        return $trad;
      }

      //// Récupère la localisation par rapport à l'adresse IP (ne fonctionne pas en local évidémment oupsi)
      function get_localisation() {
        $ip = $_SERVER['REMOTE_ADDR'];
        echo $ip;
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        echo $details;
      }

      //// CHOIX DU DOSSIER
      $dir='dossier_exemple';
      $dir='doss_simple';
      $dir='Angouleme';
      // $dir='TEST_angou';
      $contenu = getSubDirectories($dir);

      ////enlève le nom du dossier principal au début de la liste
      array_shift($contenu);

      ////crée une array associative qui récupère les infos nécessaires pour chaque fichier
      //// il faut enlever le nom du dossier de base des listes 'path' et 'dirname' (redondant et pose des problèmes pour les liens vers les fichiers)
      foreach ($contenu as $file) {
          $infos_fichier = pathinfo($file);
          $mimeType = mime_content_type($file);
          $filesize = filesize($file);
            array_push($liste_fichiers['path'], $file);
          array_push($liste_fichiers['basename'], $infos_fichier['basename']);
          array_push($liste_fichiers['dirname'], $infos_fichier['dirname']);
          array_push($liste_fichiers['mime'], $mimeType);
          array_push($liste_fichiers['categorie'], explode('/', $mimeType)[0]);
          array_push($liste_fichiers['size'], $filesize);
          array_push($liste_fichiers['modif'], date("d F Y à H\hi", filemtime($file)));

          if (is_dir($file)) {
              array_push($liste_fichiers['extension'], 'DIR');
          } else {
              if (!isset($infos_fichier['extension'])) {
                  array_push($liste_fichiers['extension'], '(null)');
              } else {
                  array_push($liste_fichiers['extension'], $infos_fichier['extension']);
              }
          }

          $profondeur = substr_count($file, '/')-1;
          array_push($liste_fichiers['profondeur'], $profondeur);
      }

      //// supprime les dossiers de la liste des types mime
      $liste_mime_clean = array_diff($liste_fichiers['mime'], array('directory'));

      //// compter les categories de fichiers (sans les dossiers)
      $count_total = count($liste_mime_clean);
      $count_extensions = array_count_values($liste_mime_clean);

      $liste_fichiers['categorie'] = array_diff($liste_fichiers['categorie'], array('directory'));
      $count_categories = array_count_values($liste_fichiers['categorie']);

      asort($liste_mime_clean);
      asort($liste_fichiers['mime']);

      //// Construit des arrays pour ranger les types de fichiers
      $arr_couv = [];
      $sum_temp = [];
      $sum_size = [];

      //// On crée les arrays pour les remplir par la suite
      foreach ($count_categories as $cat => $count) {
        $$cat = []; //// Sert uniquement de manière temporaire
        $arr_couv[$cat] = [];
        $cat_count[$cat] = 0;
      }

      //// On crée encore des arrays pour pouvoir les remplir
      $zoubida = []; // pour avoir la taille de chaque catégorie
      foreach ($liste_mime_clean as $key => $mime) {
        $cat = explode('/', $mime)[0];
        $sum_temp[$mime] = [];
        $zoubida[$cat] = [];
      }

      //// On range tous les fichiers par categorie et par type
      foreach ($liste_mime_clean as $key => $mime) {
        $cat = explode('/', $mime)[0];
        array_push($$cat, $liste_fichiers['mime'][$key]);
        array_push($sum_temp[$mime], $liste_fichiers['size'][$key]);
        array_push($zoubida[$cat], $liste_fichiers['size'][$key]);
      }

      foreach ($zoubida as $cat => $array) {
        $zoubida[$cat] = array_sum($array);
      }

      //// On compte le nombre de fichiers par type dans chaque categorie
      foreach ($count_categories as $cat => $count) {
        $arr_couv[$cat] = array_count_values($$cat);
        $cat_count[$cat] = $count;
      }

      //// On compte la taille de chaque fichier
      foreach ($count_extensions as $type => $count) {
        $sum_size[$type] = FileSizeConvert(array_sum($sum_temp[$type]));
      }

      //// PAGE SIZE
      // Les pages sont toutes entre le A4 et le A5 et restent dans la proportion sqrt(2)
      // 87 est la distance entre la hauteur du A4 et du A5
      // $max_prof - 1 est le nombre de tailes de pages intermédiaires qu'il faut calculer
      // On rajoute des marges pour compenser et avoir plusieurs tailles de pages dans le même fichier
      $max_prof = max($liste_fichiers['profondeur']);

      $dimensions = [];
      $dimensions[1] = [210, 297];
      if ($max_prof > 1) {
        echo '<style>';
        $pas = 87 / $max_prof;
        for ($i = 2; $i <= $max_prof + 1; $i++) {
          $hauteur = 297 - (($i - 1) * $pas);
          $largeur = round($hauteur / sqrt(2), 1);
          $margins_vert = 297 - $hauteur;
          $margins_horiz = 210 - $largeur;
          // echo '<pre>';
          echo '@page prof'.($i - 1).' {margin-top: '.($margins_vert / 2).'mm; margin-right: '.($margins_horiz / 2).'mm; margin-bottom: '.($margins_vert / 2).'mm; margin-left: '.($margins_horiz / 2).'mm; --hauteur: '.$hauteur.'mm;}';
          echo '.prof'.($i-1).' {page: prof'.($i-1).';}';

          $dimensions[$i] = [];
          array_push($dimensions[$i], $largeur);
          array_push($dimensions[$i], $hauteur);
        };
        echo '</style>';
      }

      //// FORMAT BOÎTE
      $count_livrets = array_count_values($liste_fichiers['mime']);
      $dossiers = $count_livrets['directory'];
      $fichiers = array_sum($count_livrets) - $dossiers;

      //// BLBLBLBLBBLBBL
      $copie_dirname = $liste_fichiers['dirname'];
      asort($copie_dirname);
      foreach (array_keys($liste_fichiers['mime'], 'directory') as $key => $a_supp) {
        unset($copie_dirname[$a_supp]);
        // echo $liste_fichiers['path'][$a_supp].'<br />';
      }

      $fichiers_par_dossier = array_count_values($copie_dirname);
    ?>
  </head>

  <body>
    <!-- <section id="couv" class="page sticker">
      <div class="coin coin_top"></div>
      <div class="coin coin_right"></div>
      <div class="coin coin_bottom"></div>
      <div class="coin coin_left"></div>
      <div class="sticker_wrapper">
      <?php
        echo '<h1>'.$dir.'</h1>';
        $current_date = new DateTime();
        $jour_ouverture = $current_date->format('d/m/Y à H\hi');
        echo '<p>Ouvert le '.$jour_ouverture.'</p>';
        echo '<p>Taille&thinsp;:  '.FileSizeConvert(array_sum($zoubida)).'</p>';
      ?>
        <table id="types">
          <?php
            //// On affiche chaque catégorie de fichier, chaque type avec son poids par rapport au poids total des fichiers
            foreach ($arr_couv as $categorie => $value) {
              if ($categorie == 'text') {
                $titre = 'textes';
              } else if ($categorie == 'video') {
                $titre = 'vidéos';
              } else {
                $titre= $categorie.'s';
              }

              $prct = ($zoubida[$categorie] / array_sum($zoubida)) * 100;
              echo '<tr>';
              echo '<th colspan="2">'.ucfirst($titre).' <span class="pabold">'.FileSizeConvert($zoubida[$categorie]).'</span></th><th class="pourcent">'.round($prct, 1).'%</th>';
              echo '</tr>';
              foreach ($arr_couv[$categorie] as $type => $size) {
                $friendlyName = JCoded\FriendlyMime\FriendlyMime::getFriendlyName($type); //// Convertit le type MIME en un nom lisible par un·e humain·e
                echo '<tr class="type">';
                echo '<td class="alinéa">oo</td><td>'.$friendlyName.' '.$sum_size[$type].'</td><td></td>';
                echo '</tr>';
              }
            }
          echo '</table>';

          echo '<table id="formats">';
          echo '<tr><th></th><th class="format_largeur">Largeur</th><th class="format_hauteur">Hauteur</th></tr>';
            foreach ($dimensions as $key => $dim) {
              echo '<tr>';
              echo '<td>Format '.$key.'</td>';
              echo '<td class="format_largeur">'.$dim[0].'&thinsp;mm</td>';
              echo '<td class="format_hauteur">'.$dim[1].'&thinsp;mm</td>';
              echo '</tr>';
            }
          echo '</table>';

          echo '<table id="nbres">';
            echo'<tr>';
              echo '<td>'.$dossiers.' dossiers</td>';
              echo '<td class="table-separator">/</td>';
              echo '<td>'.$fichiers.' fichiers</td>';
            echo'</tr>';
          echo '</table>';
        ?>

      </div>
    </section> -->

    <!-- <section id="colophon" class="page sticker">
      <div class="coin coin_top"></div>
      <div class="coin coin_right"></div>
      <div class="coin coin_bottom"></div>
      <div class="coin coin_left"></div>

      <div class="sticker_wrapper">
        <?php
        $personne = 'Thomas Bris';
        $ville = 'Bruxelles';
        $user_agent = get_browser(null, true);
        $constructeur = explode(' ', $user_agent['browser_maker'])[0];
        $description = 'Un bot Twitter qui se balade au hasard dans les villages de la campagne française. Dans chaque post, le bot annonce le village dans lequel il compte se rendre, accompagné de la distance en km et du temps de trajet estimé (le bot se balade à pied). Ce temps de trajet correspond au temps qui va s’écouler avant le prochain post, c’est-à-dire le temps que le bot arrive dans le village et choisisse sa prochaine destination. Le choix de la commune se fait au hasard parmi les villages se trouvant dans un rayon de plus ou moins 25 km autour de l’endroit où se trouve le bot actuellement.';

        echo '<p>';
        echo '<b>Objet généré à partir du contenu du dossier '.$dir.', récupéré sur l\'ordinateur personnel de '.$personne.' le '.trad($current_date->format('l d F Y à H\hi')).' à '.$ville.', produit depuis un navigateur '.$constructeur.' '.$user_agent['browser'].' version '.$user_agent['version'].', dans d\'un système d\'exploitation '.$user_agent['platform'].'.</b>';
        echo '</p>';

        echo '<article>';
        echo '<p>INGRÉDIENTS&thinsp;: fontes sans-serif et mono&shyspace</b> par défaut sur l\'ordinateur, papier 300g/m² (boîte), papier 120g/m² (intérieur), reliures en rivets et oeillets, impression jet d\'encre couleur (FacOne Copy, Bruxelles) CONTIENT&thinsp;: '.$description.'</p>';
        echo '</article>';

        echo '<p class="programme">';
        echo 'Le code pour générer le livre a été écrit par Thomas Bris. Il est accessible par ici&thinsp;:<br />&rarr;<span class="url">https://gitlab.com/ham-bistros/votre-projeeet</span>';
        echo '</p>';

        ?>
      </div>
    </section> -->

    <!-- <section id="index">
      <h1>INDEX</h1>
      <?php
        //// On regarde chaque fichier et on affiche que les dossiers
        //// On trie la liste pour que les sous-dossiers se retrouvent bien sous leur dossier parent
        //// Avec des trickzzz de borders on peut visualiser l'arborescence
        $copie = $liste_fichiers['path'];
        asort($copie);
        echo '<h2>'.$dir.'</h2>';
        echo '<article>';
        $compteur_fichiers = 0;

        foreach ($fichiers_par_dossier as $dossier => $nb_fichiers) {
          $depth = substr_count($dossier, '/') - 1;
          $padding = $depth * 1;

          if ($depth > 0) {
            // echo '<p style="margin-left: '.$padding.'em; border-left: var(--border);">';
            echo '<p style="margin-left: '.$padding.'em; border-left: solid black 1.5pt;">';
          } else {
            echo '<p>';
          }
            $alors = explode('/', $dossier);

            if ($depth != -1) {
              // si $depth == -1 on est dans le dossier parent et pas un sous-dossier
              echo '<span class="nom-dossiers">'.$alors[count($alors) - 1].'</span>';
            }

            echo '<span class="comblage">m</span>';
            if ($nb_fichiers != 1) {
              echo '<span class="nb-fichiers">'.$nb_fichiers.' fichiers</span>';
            } else {
              echo '<span class="nb-fichiers">'.$nb_fichiers.' fichier</span>';
            }
          echo '</p>';
        }
        echo '</article>';
      ?>
    </section> -->

    <section id="content">
      <?php
        foreach ($liste_fichiers['path'] as $key => $file) {
          if ($liste_fichiers['mime'][$key] != 'directory') {

            //// Affiche les infos de chaque fichier dans une page
            echo '<section class="file infos prof'.$liste_fichiers['profondeur'][$key].'">';

              //// CROP MARKS
              if ($liste_fichiers['profondeur'][$key] >= 1) {
                echo '<div class="coin coin_top"></div>';
                echo '<div class="coin coin_right"></div>';
                echo '<div class="coin coin_bottom"></div>';
                echo '<div class="coin coin_left"></div>';
              }

              $dirname = str_replace($dir, '', $liste_fichiers['dirname'][$key]);
              if ($dirname == '') {
                $dirname = '/';
              }
              $path = str_replace($dir.'/', '', $liste_fichiers['path'][$key]);
              $path = $liste_fichiers['basename'][$key];
              $size = FileSizeConvert($liste_fichiers['size'][$key]);
              echo '<h2 class="fichier nom_parent">'.$dirname.'</h2>';
              echo '<h3 class="nom_fichier">Nom: '.$path.'</h3>';
              echo '<p>Type: '.$liste_fichiers['mime'][$key].'</p>';
              echo '<p>Taille: '.$size.'</p>';
              echo '<p>Dernière modification: '.trad($liste_fichiers['modif'][$key]).'</p>';
            echo '</section>';

            //// Affiche les un aperçu de chaque fichier au verso
            echo '<section class="file apercu prof'.$liste_fichiers['profondeur'][$key].'">';
              if ($liste_fichiers['categorie'][$key] == 'text' || $liste_fichiers['categorie'][$key] == 'application') {
                if ($liste_fichiers['extension'][$key] == 'html') {
                  $html_escaped = htmlspecialchars(file_get_contents($file, FALSE, NULL, 0, 3500));
                  echo '<h2 class="fichier nom_apercu">🌐</h2>';
                  echo '<pre>'.$html_escaped.'<hr /></pre>';
                } else {
                  echo '<h2 class="fichier nom_apercu">🖹</h2>';
                  echo '<pre>'.file_get_contents($file, FALSE, NULL, 0, 3500).'<hr /></pre>';
                }

              } else if ($liste_fichiers['categorie'][$key] == 'image') {
                echo '<h2 class="fichier nom_apercu">🖻</h2>';
                echo '<img src="'.$file.'">';

              } else if ($liste_fichiers['categorie'][$key] == 'video') {
                echo '<h2 class="fichier nom_apercu">🖵</h2>';
                echo '<video src="'.$file.'"></video>';

              } else {
                echo '<p>'.$liste_fichiers['mime'][$key].'</p>';
              }
            echo "</section>";
          }
        }
      ?>
    </section>


    <div id="invisible">
      <p id="fichiers"><?php echo $fichiers; ?></p>
      <p id="dossiers"><?php echo $dossiers; ?></p>
    </div>
    <script type="text/javascript" src="script/script.js"></script>
  </body>
</html>
