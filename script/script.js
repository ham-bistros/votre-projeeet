/* Calcule le format de la boîte en fonction du nombre de pages / de livrets */

const epaisseurPapier = 0.1; // pour du papier 100g/m² environ
const epaisseurRivet = 2; // pour une certaine taille de rivets
const nbrFeuilles = document.getElementById('fichiers').innerHTML;
const nbrLivrets = document.getElementById('dossiers').innerHTML;

var sizeA = [298, 211];
var sizeD = epaisseurPapier * nbrFeuilles + epaisseurRivet * nbrLivrets + 1;
var sizeD = 14 + 1;
var sizeF = [299, 211];

console.log('Toutes les mesures sont en mm');
console.log('Hauteur / Largeur');
console.log('A', sizeA[0], sizeA[1]);
console.log('B = A');
console.log('C', sizeA[1] / 4);
console.log('D', sizeD);
console.log('E', sizeD + 1);
console.log('F', sizeA[0] + 1, sizeA[1]);
console.log('G = F');
console.log('H', sizeF[0]/4);
console.log('I', sizeD + 1);

console.log(' ');
largeur1 = 2*sizeA[1] + sizeA[1]/4 + 3*sizeD + 1;
console.log('Bande 1 -> ', Math.round(largeur1));


console.log(' ');
largeur2 = 2*sizeF[0] + 2*sizeD + 2 + sizeF[0]/4;
console.log('Bande 2 -> ', Math.round(largeur2));


// var sizeA = [298, 211];
// // var sizeD = epaisseurPapier * nbrFeuilles + epaisseurRivet * nbrLivrets + 1;
// var sizeD = 14 + 1;
// var sizeF = [299, 211];
//
// const mi = .297;
//
// console.log('Toutes les mesures sont en mm');
// console.log('Hauteur / Largeur');
// console.log('A', sizeA[0]*mi, sizeA[1]*mi);
// console.log('B = A');
// console.log('C', sizeA[1]*mi / 4);
// console.log('D', sizeD*mi);
// console.log('E', sizeD*mi + 1*mi);
// console.log('F', sizeA[0]*mi + 1*mi, sizeA[1]*mi);
// console.log('G = F');
// console.log('H', sizeF[0]*mi/4);
// console.log('I', sizeD*mi + 1*mi);
//
// console.log(' ');
// largeur1 = 2*sizeA[1]*mi + sizeA[1]*mi/4 + 3*sizeD*mi + 1*mi;
// console.log('Bande 1 -> ', Math.round(largeur1));
//
//
// console.log(' ');
// largeur2 = 2*sizeF[0]*mi + 2*sizeD*mi + 2 + sizeF[0]*mi/4;
// console.log('Bande 2 -> ', Math.round(largeur2));
